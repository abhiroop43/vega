import { MakesService } from './../makes.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { IMake } from '../make';

@Component({
  selector: 'app-create-make',
  templateUrl: './create-make.component.html',
  styleUrls: ['./create-make.component.css']
})
export class CreateMakeComponent implements OnInit {
  makeForm: FormGroup;
  constructor(private fb: FormBuilder, private makeService: MakesService, private router: Router) {}

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.makeForm = this.fb.group({ name: ['', Validators.required] });
  }

  onSubmit() {
    const make: IMake = { id: 0, name: this.makeForm.value['name'], models: [] };
    this.makeService.addNewMake(make).subscribe(
      res => {
        console.log('New make created successfully', res);
        if (res > 0) {
          this.router.navigate(['/makes']);
        }
      },
      err => {
        console.warn('Error occurred while adding new make', err);
      }
    );
  }
}
