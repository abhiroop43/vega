import { MakesService } from './../makes.service';
import { Component, OnInit } from '@angular/core';
import { IMake } from '../make';

@Component({
  selector: 'app-makes-list',
  templateUrl: './makes-list.component.html',
  styleUrls: ['./makes-list.component.css']
})
export class MakesListComponent implements OnInit {
  makes: IMake[];
  constructor(private makesService: MakesService) {}

  ngOnInit() {
    this.makesService.getAllMakes().subscribe((makes: IMake[]) => {
      this.makes = makes;
    });
  }
}
