import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IMake } from './make';

@Injectable()
export class MakesService {
  baseUrl = 'https://localhost:5001/api/';
  constructor(private http: HttpClient) {}

  getAllMakes() {
    const httpOptions = {
      headers: new HttpHeaders({
        Accept: 'application/json'
      })
    };

    return this.http.get<IMake[]>(`${this.baseUrl}Makes/GetAllMakes`, httpOptions);
  }

  addNewMake(newMake: IMake) {
    const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };

    return this.http.post(`${this.baseUrl}Makes/AddNewMake`, newMake, httpOptions);
  }

  updateMake(make: IMake) {
    const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };

    return this.http.put(`${this.baseUrl}Makes/UpdateMake`, make, httpOptions);
  }

  deleteMake(makeId: number) {
    const httpOptions = { headers: new HttpHeaders({ Accept: 'application/json' }) };

    return this.http.delete(`${this.baseUrl}Makes/DeleteMake/${makeId}`, httpOptions);
  }
}
