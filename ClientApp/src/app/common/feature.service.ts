import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IFeature } from './feature';
@Injectable()
export class FeatureService {
  baseUrl = 'https://localhost:5001/api/';
  constructor(private http: HttpClient) {}

  getAllFeatures() {
    const httpOptions = { headers: new HttpHeaders({ Accept: 'application/json' }) };

    return this.http.get<IFeature[]>(`${this.baseUrl}Features/GetAllFeatures`, httpOptions);
  }
}
