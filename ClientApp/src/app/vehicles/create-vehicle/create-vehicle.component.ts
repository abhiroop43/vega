import { FeatureService } from './../../common/feature.service';
import { IModel } from './../../makes/model';
import { Router } from '@angular/router';
import { MakesService } from './../../makes/makes.service';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { IMake } from '../../makes/make';
import { IFeature } from '../../common/feature';

@Component({
  selector: 'app-create-vehicle',
  templateUrl: './create-vehicle.component.html',
  styleUrls: ['./create-vehicle.component.css']
})
export class CreateVehicleComponent implements OnInit {
  vehicleForm: FormGroup;
  makes: IMake[] = [];
  models: IModel[] = [];
  features: IFeature[] = [];

  constructor(
    private fb: FormBuilder,
    private makeService: MakesService,
    private featureService: FeatureService,
    private router: Router
  ) {}

  ngOnInit() {
    this.makeService.getAllMakes().subscribe(
      (makes: IMake[]) => {
        console.log('Response returned', makes);
        this.makes = makes;
      },
      err => {
        console.warn('Error occurred while fetching makes', err);
      }
    );

    this.featureService.getAllFeatures().subscribe(
      (features: IFeature[]) => {
        console.log('Features returned', features);
        this.features = features;
      },
      err => {
        console.warn('Error occurred while getting features', err);
      }
    );
    this.createForm();
    this.selectedMakeChanged();
  }

  createForm() {
    this.vehicleForm = this.fb.group({
      make: ['', Validators.required],
      model: ['', Validators.required],
      isRegistered: [null, Validators.required],
      features: new FormArray([]),
      contact: this.fb.group({
        name: ['', Validators.required],
        phone: ['', Validators.required],
        email: ['', [Validators.required, Validators.email]]
      })
    });
  }

  selectedMakeChanged() {
    this.vehicleForm.valueChanges.subscribe(val => {
      // console.log(val);
      const selectedMakeId = +val.make; // converting to a number
      // console.log(selectedMakeId);
      const foundMake = this.makes.find(m => m.id === selectedMakeId);
      if (foundMake !== undefined) {
        this.models = foundMake.models;
      }
    });
  }

  onCheckChange(event) {
    const formArray: FormArray = this.vehicleForm.get('features') as FormArray;

    /* Selected */
    if (event.target.checked) {
      // Add a new control in the arrayForm
      formArray.push(new FormControl(+event.target.value));
    } else {
      // find the unselected element
      let i = 0;

      formArray.controls.forEach((ctrl: FormControl) => {
        if (ctrl.value === event.target.value) {
          // Remove the unselected element from the arrayForm
          formArray.removeAt(i);
          return;
        }

        i++;
      });
    }
  }

  onSubmit() {
    console.log('Submitted form value', this.vehicleForm.value);
  }
}
