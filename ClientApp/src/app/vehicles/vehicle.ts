export interface IVehicle {
  make: number;
  model: number;
  features: number[];
  isRegistered: boolean;
  contact: IContact;
}

export interface IContact {
  name: string;
  phone: string;
  email: string;
}
