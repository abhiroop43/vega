import { FeatureService } from './common/feature.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { CounterComponent } from './counter/counter.component';
import { FetchDataComponent } from './fetch-data/fetch-data.component';
import { MakesListComponent } from './makes/makes-list/makes-list.component';
import { CreateMakeComponent } from './makes/create-make/create-make.component';
import { MakesService } from './makes/makes.service';
import { VehiclesListComponent } from './vehicles/vehicles-list/vehicles-list.component';
import { CreateVehicleComponent } from './vehicles/create-vehicle/create-vehicle.component';
import { AppRoutingModule } from './app-routing.module';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    CounterComponent,
    FetchDataComponent,
    MakesListComponent,
    CreateMakeComponent,
    VehiclesListComponent,
    CreateVehicleComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule
  ],
  providers: [MakesService, FeatureService],
  bootstrap: [AppComponent]
})
export class AppModule {}
