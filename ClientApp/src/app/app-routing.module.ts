import { CreateVehicleComponent } from './vehicles/create-vehicle/create-vehicle.component';
import { VehiclesListComponent } from './vehicles/vehicles-list/vehicles-list.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { CounterComponent } from './counter/counter.component';
import { FetchDataComponent } from './fetch-data/fetch-data.component';
import { MakesListComponent } from './makes/makes-list/makes-list.component';
import { CreateMakeComponent } from './makes/create-make/create-make.component';

const appRoutes: Routes = [
  { path: '', component: HomeComponent, pathMatch: 'full' },
  { path: 'counter', component: CounterComponent },
  { path: 'fetch-data', component: FetchDataComponent },
  { path: 'makes', component: MakesListComponent },
  { path: 'create-make', component: CreateMakeComponent },
  { path: 'vehicles', component: VehiclesListComponent },
  { path: 'create-vehicle', component: CreateVehicleComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes
      // { enableTracing: true } // <-- debugging purposes only
    )
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
