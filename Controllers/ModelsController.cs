using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using vega.Data;
using vega.Dto;

namespace vega.Controllers
{
    [Route("api/[controller]")]
    public class ModelsController : Controller
    {
        private readonly VegaDbContext _context;
        private readonly IMapper _mapper;

        public ModelsController(VegaDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        [HttpPost("[action]")]
        public async Task<int> AddNewModel([FromBody]ModelDto model)
        {
            _context.Models.Add(new Model
            {
                Name = model.Name,
                MakeId = model.MakeId
            });

            return await _context.SaveChangesAsync();
        }
    }
}