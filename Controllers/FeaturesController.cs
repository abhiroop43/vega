using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using vega.Data;
using vega.Dto;

namespace vega.Controllers
{
    [Route("api/[controller]")]
    public class FeaturesController : Controller
    {
        private readonly VegaDbContext _context;
        private readonly IMapper _mapper;

        public FeaturesController(VegaDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        [HttpGet("[action]")]
        public async Task<List<FeatureDto>> GetAllFeatures()
        {
            var features = await _context.Features.ToListAsync();
            List<FeatureDto> retVal = new List<FeatureDto>();
            if (features.Any())
            {
                foreach (var feat in features)
                {
                    var featDto = _mapper.Map<Feature, FeatureDto>(feat);
                    retVal.Add(featDto);
                }
            }

            return retVal;
        }

        [HttpPost("[action]")]
        public async Task<int> AddNewFeature([FromBody] FeatureDto feature)
        {
            Feature newFeat = new Feature
            {
                Name = feature.Name
            };

            _context.Features.Add(newFeat);

            return await _context.SaveChangesAsync();
        }
    }
}