using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using vega.Data;
using vega.Dto;

namespace vega.Controllers
{
    [Route("api/[controller]")]
    public class MakesController : Controller
    {
        private readonly VegaDbContext _context;
        private readonly IMapper _mapper;

        public MakesController(VegaDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        [HttpGet("[action]")]
        public async Task<IEnumerable<MakeDto>> GetAllMakes()
        {
            var allMakes = await _context.Makes.Include(m => m.Models).ToListAsync();

            List<MakeDto> retVal = new List<MakeDto>();
            if (allMakes.Any())
            {
                foreach (var make in allMakes)
                {
                    var makeDto = _mapper.Map<Make, MakeDto>(make);
                    retVal.Add(makeDto);
                }
            }

            return retVal;
        }

        [HttpPost("[action]")]
        public async Task<int> AddNewMake([FromBody] MakeDto make)
        {
            _context.Makes.Add(new Make
            {
                Name = make.Name
            });

            return await _context.SaveChangesAsync();
        }

        [HttpPut("[action]")]
        public async Task<int> UpdateMake([FromBody] MakeDto make)
        {
            var oldMake = await _context.Makes.FindAsync(make.Id);

            if (oldMake != null)
            {
                oldMake.Name = make.Name;
                _context.Entry(oldMake).State = EntityState.Modified;
            }

            return await _context.SaveChangesAsync();
        }

        [HttpDelete("[action]")]
        public async Task<int> DeleteMake(int makeId)
        {
            var currentMake = await _context.Makes.FindAsync(makeId);

            if (currentMake != null)
            {
                _context.Entry(currentMake).State = EntityState.Deleted;
            }

            return await _context.SaveChangesAsync();
        }
    }
}