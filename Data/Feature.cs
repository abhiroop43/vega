using System.ComponentModel.DataAnnotations;

namespace vega.Data
{
    public class Feature
    {
        [Key]
        public int Id { get; set; }

        [MaxLength(255)]
        [Required]
        public string Name { get; set; }
    }
}