using System.ComponentModel.DataAnnotations;

namespace vega.Data
{
    public class Model
    {
        [Key]
        public int Id { get; set; }

        [MaxLength(255)]
        [Required]
        public string Name { get; set; }

        public int MakeId { get; set; }

        public Make Make { get; set; }
    }
}