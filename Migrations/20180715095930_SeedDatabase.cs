﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace vega.Migrations
{
    public partial class SeedDatabase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("INSERT INTO Makes (Name) Values ('Audi')");
            migrationBuilder.Sql("INSERT INTO Makes (Name) Values ('Cadillac')");
            migrationBuilder.Sql("INSERT INTO Makes (Name) Values ('Bentley')");

            migrationBuilder.Sql("INSERT INTO Models (Name, MakeId) Values ('A7', (SELECT Id FROM Makes where Name = 'Audi'))");
            migrationBuilder.Sql("INSERT INTO Models (Name, MakeId) Values ('TT', (SELECT Id FROM Makes where Name = 'Audi'))");
            migrationBuilder.Sql("INSERT INTO Models (Name, MakeId) Values ('Escalade', (SELECT Id FROM Makes where Name = 'Cadillac'))");
            migrationBuilder.Sql("INSERT INTO Models (Name, MakeId) Values ('ATS', (SELECT Id FROM Makes where Name = 'Cadillac'))");
            migrationBuilder.Sql("INSERT INTO Models (Name, MakeId) Values ('Continental', (SELECT Id FROM Makes where Name = 'Bentley'))");
            migrationBuilder.Sql("INSERT INTO Models (Name, MakeId) Values ('Bentayga', (SELECT Id FROM Makes where Name = 'Bentley'))");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            // migrationBuilder.Sql("TRUNCATE TABLE Models");
            migrationBuilder.Sql("DELETE FROM Makes WHERE Name IN ('Audi', 'Cadillac', 'Bentley')");
            // migrationBuilder.Sql("DBCC CHECKIDENT ('vega.dbo.Makes',RESEED, 0)");
        }
    }
}
